import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ManagerPage from "./component/page/ManagerPage";
import SalesPage from "./component/page/SalesPage";
import Login from "./component/page/LoginPage";
import AddReport from './component/addReport/addReport'

class App extends Component {
  render() {
    return (
      <>
        <Router>
          <Switch>
            <Route path="/" exact component={Login} />
            <Route path="/Manager" component={ManagerPage}>
              <ManagerPage />
            </Route>
            <Route path="/Sales" component={SalesPage}>
              <SalesPage />
            </Route>
          </Switch>
        </Router>
      </>
    );
  }
}

export default App;
