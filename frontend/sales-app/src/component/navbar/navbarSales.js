import React, { Component } from 'react'
import {Link, Route} from 'react-router-dom'
import addReport from '../addReport/addReport'
import LoginPage from '../page/LoginPage'

export default class navbarSales extends Component {
    render() {
        return (
            <>
                <div className="navbar navbar-dark  shadow-sm">
                    <a href="#" className="navbar-brand col-md-7">
                        <h2 className="font-weight-bold maincolor">Sales Report</h2>
                    </a>
                    <span className="navbar-brand col-md-3">
                        <a href="/sales/addreport" className="link">
                            <button type="button" className="col-md-5 btn mainbackground text-light font-weight-semibold"><i class="fas fa-plus"></i> add report</button>
                        </a>
                        <span className = "col-md-2"></span>
                        <Link to = "/login">
                            <button type="button" className="col-md-5 btn mainbackground text-light font-weight-semibold">Logout</button>
                        </Link>
                    </span>
                </div>
            </>
        )
    }
}
