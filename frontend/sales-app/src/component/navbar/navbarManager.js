import React from 'react';
import '../../App.css';
import { Link } from 'react-router-dom';

function nav() {
    return (
        <>
            <div className="navbar navbar-dark  shadow-sm">
                <a href="#" className="navbar-brand col-md-7">
                    <h2 className="font-weight-bold maincolor">Sales Report</h2>
                </a>
                <span className="navbar-brand col-md-3">
                        <button type="button" className="col-md-5 btn mainbackground text-light font-weight-semibold">Stock</button>
                        <span className = "col-md-2"></span>
                        <Link to = "/login">
                        <button type="button" className="col-md-5 btn mainbackground text-light font-weight-semibold">Logout</button>
                        </Link>
                </span>
            </div>
        </>
    );
}


export default nav;