import React, { Component } from 'react'
import Logo from '../../paper.png'

export default class addReport extends Component {
    render() {
        return (
            <div>
                <div className="row mt-4">
                    <div className="offset-md-2 col-md-8">
                        <div class="jumbotron">
                            <div className="text-center">
                                <img src={Logo} alt="" className="img-logo-add" width="210vw"/>
                            </div>
                            <form className="mt-5">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1">Product Name</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>Skirt</option>
                                        <option>Jacket</option>
                                        <option>Pants</option>
                                        <option>Sweater</option>
                                        <option>T-shirt</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Report Message</label>
                                    <input type="text" class="form-control" id="" placeholder="" />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="">Quantity</label>
                                    <input className="form-control" placeholder=""/>
                                </div>
                                <div className="form-group ">
                                    <button className="btn btn-primary">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
