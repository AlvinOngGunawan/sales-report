import React, { Component } from 'react'

export default class sidebar extends Component {
    render() {
        return (
            <div>
                <div class="bg-light border-right text-center" id="sidebar-wrapper">
                    <div class="sidebar-heading">
                        <p className="icon mt-4 mb-4"><i class="far fa-user-circle d-block"></i></p>
                        <p className="mt-3">Samuel Marsellino</p>
                    </div>
                    <div class="list-group list-group-flush">
                        <a href="#" class="list-group-item list-group-item-action bg-light mt-3">
                            <p><i class="fas fa-tachometer-alt"></i></p>
                            Dashboard
                        </a>
                        <a href="#" class="list-group-item list-group-item-action bg-light mt-5">
                            <p><i class="fas fa-history"></i></p>
                            History
                        </a>
                        <a href="#" class="list-group-item list-group-item-action bg-light mt-5">
                            <p><i class="far fa-chart-bar"></i></p>    
                            Chart
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}
