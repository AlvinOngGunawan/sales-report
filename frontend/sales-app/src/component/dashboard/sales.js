import React from 'react';
import '../../App.css';

function Dashboard() {
    return (
        <>
            <div className="album mt-5">
                <div className="align-items-center justify-content-between">
                    <div className="row">
                        <div className="col-md-12 mb-5">
                             <h3>Sales</h3>
                        </div>
                        <div className="col-md-4">
                            <div className="card mb-4 shadow-sm red-card">
                                <div className="card-body">
                                    <h2 className="card-text text-light mb-3">Product A</h2>
                                    <p className="card-text text-light mb-4">Total Report : 24</p>
                                    <div className="d-flex justify-content-between align-items-center float-right">
                                        <div className="btn-group">
                                            <button type="button" className="btn buttoncard text-light">Details</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="card mb-4 shadow-sm red-card">
                                <div className="card-body">
                                    <h2 className="card-text text-light mb-3">Product B</h2>
                                    <p className="card-text text-light mb-4">Total Report : 24</p>
                                    <div className="d-flex justify-content-between align-items-center float-right">
                                        <div className="btn-group">
                                            <button type="button" className="btn buttoncard text-light">Details</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="card mb-4 shadow-sm red-card">
                                <div className="card-body">
                                    <h2 className="card-text text-light mb-3">Product C</h2>
                                    <p className="card-text text-light mb-4">Total Report : 24</p>
                                    <div className="d-flex justify-content-between align-items-center float-right">
                                        <div className="btn-group">
                                            <button type="button" className="btn buttoncard text-light">Details</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>

    );
}


export default Dashboard;