import React from 'react';
import Nav from '../navbar/navbarManager';
import Dashboard from '../dashboard/manager.js';
import Filter from '../filter';
import Sidebar from '../sidebar/SidebarManager'
import DetailManager from '../details/detailManager';
import HistoryManager from '../history/historyManager';

function ManagerPage() {
    return (
        <>
            <Nav />
            <div className="row container-fluid">
                <div className="col-md-2">
                    <Sidebar />
                </div>
                <div className="col-md-10">
                    {/* <Filter /> */}
                    {/* <Dashboard /> */}
                    <HistoryManager />
                </div>
            </div>
        </>

    );
}

export default ManagerPage;