import React from 'react';
import Dashboard from '../dashboard/sales.js';
import Filter from '../filter';
import Sidebar from '../sidebar/sidebarSales'
import DetailSales from '../details/detailSales'
import Nav from '../navbar/navbarSales'
import AddReport from '../addReport/addReport'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'

function ManagerPage() {
    return (
        <>
            <Nav />
            <div className="row container-fluid">
                <div className="col-md-2">
                    <Sidebar />
                </div>
                <div className="col-md-10">
                    <Router>
                        <Switch>
                            <Route path="/sales" exact component={Dashboard} />
                            <Route path="/sales/history"  component={DetailSales} />
                            <Route path="/sales/addreport" component={AddReport} />
                        </Switch>
                    </Router>
                    {/* <Filter /> */}
                    {/* <Dashboard />
                    <DetailSales /> */}
                    {/* <AddReport /> */}
                </div>
            </div>
        </>

    );
}

export default ManagerPage;