package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "history")
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id_history;

    @Column( nullable = false)
    private Integer sold_total;

    @Column( nullable = false)
    private Date time;

    @Column( nullable = false)
    private String message;

    @PrePersist
    protected void onCreate(){
        this.time = new Date();
    }

//    public Integer getIdFk() {
//        return this.id_fk;
//    }
}
