package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "sales")
public class Sales {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false)
    private Integer id_sales;

    @Column(nullable = false)
    private String name;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

//    @Column( nullable = false)
//    private String position;

    @OneToMany(targetEntity = History.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_fk", referencedColumnName = "id_sales")
    private List<History> history;

    @OneToMany(targetEntity = Goods.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_fk", referencedColumnName = "id_sales")
    private List<Goods> goods;


    public List<History> getHistory() {
        return history;
    }

    public Integer getId() {
        return this.id_sales;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
//    private Sales sales;

//    public List<History> getHistory();


