package com.example.demo.service;

import com.example.demo.entity.Sales;
import com.example.demo.repository.SalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesService {
    @Autowired
    private SalesRepository sales;

    public Sales getSalesById(int id){
        return sales.findById(id).orElse(null);
    }
}
