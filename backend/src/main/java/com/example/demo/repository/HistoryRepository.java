package com.example.demo.repository;

import com.example.demo.dto.DTOResponse;
import com.example.demo.entity.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface HistoryRepository extends JpaRepository<History, Integer> {
//    History findById(String id_history);
    @Query("SELECT new com.example.demo.dto.DTOResponse(a.id_sales, a.name, a.username, a.password, b.id_history, b.message, b.time, b.sold_total, " +
            "c.id_goods, c.name_goods, c.stock_goods) FROM Sales a JOIN a.history b JOIN a.goods c")
    public List<DTOResponse> getAllDataSales();


}
