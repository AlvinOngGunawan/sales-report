package com.example.demo.repository;

import com.example.demo.dto.DTOResponse;
import com.example.demo.entity.Sales;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SalesRepository extends JpaRepository<Sales, Integer> {

    @Query("SELECT new com.example.demo.dto.DTOResponse(a.id_sales, a.name, a.username, a.password, b.id_history, b.message, b.time, b.sold_total, " +
            "c.id_goods, c.name_goods, c.stock_goods) FROM Sales a JOIN a.history b JOIN a.goods c")
    public List<DTOResponse> getAllDataSales();


    @Query("SELECT new com.example.demo.dto.DTOResponse(a.id_sales, a.name, a.username, a.password, b.id_history, b.message, b.time, b.sold_total, " +
            "c.id_goods, c.name_goods, c.stock_goods) FROM Sales a JOIN a.history b JOIN a.goods c WHERE a.name LIKE %:name%")
    public List<DTOResponse> getAllDataSalesByName(@Param("name") String name);

    @Query("SELECT new com.example.demo.dto.DTOResponse(a.id_sales, a.name, a.username, a.password, b.id_history, b.message, b.time, b.sold_total, " +
            "c.id_goods, c.name_goods, c.stock_goods) FROM Sales a JOIN a.history b JOIN a.goods c WHERE b.sold_total >= :first AND b.sold_total <= :second")
    public List<DTOResponse> getAllDataSalesBySoldTotal(@Param("first") Integer first,
                                                        @Param("second") Integer second);
    Sales findByUsername(String username);
}
