package com.example.demo.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class DTOResponse {
    private Integer id_sales;
    private String name;
    private String username;
    private String password;
//    private String position;
    private Integer id_history;
    private String message;
    private Date time;
    private Integer id_goods;
    private Integer sold_total;
    private String name_goods;
    private Integer stock_goods;
}
