package com.example.demo.controller;

import com.example.demo.dto.DTORequest;
import com.example.demo.dto.DTOResponse;
import com.example.demo.dto.loginRequest;
import com.example.demo.dto.loginResponse;
import com.example.demo.entity.History;
import com.example.demo.entity.Sales;
import com.example.demo.repository.SalesRepository;
import com.example.demo.service.SalesService;
import com.example.demo.util.JWTUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class SalesController {
    @Autowired
    private SalesRepository sales;
    @Autowired
    private SalesService salesService;
    @Autowired
    private JWTUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    @PostMapping("/addSales")
    private Sales saveSales(@RequestBody DTORequest request) {
//        Sales salesRequest = request.getSales();
//        List<History> historiesRequest = salesRequest.getHistory();
//        Optional<Sales> currentSales = sales.findById(salesRequest.getId());
//
//        for (History his : currentSales.get().getHistory()) {
//            historiesRequest.add(his);
//        }
////        System.out.println(historiesRequest);
//        return sales.save(salesRequest);
        return sales.save(request.getSales());
    }

    @PostMapping("/addSalesById/{id}")
    private Sales saveSalesById(@RequestBody DTORequest request, @PathVariable int id) {
        return sales.save(salesService.getSalesById(id));
    }

    @GetMapping("/getSales")
    private List<DTOResponse> getSales(){ return sales.getAllDataSales(); }

    @GetMapping("/getSalesByName/{name}")
    private List<DTOResponse> getSalesByName(@PathVariable String name){
        return sales.getAllDataSalesByName(name);
    }

    @GetMapping("/getSalesBySoldTotal/{first}/{second}")
    public List<DTOResponse> getbysalary(@PathVariable Integer first,@PathVariable Integer second){
        return sales.getAllDataSalesBySoldTotal(first, second);
    }

    @GetMapping("/getSalesById/{id}")
    public Sales findSalesById(@PathVariable int id){ return salesService.getSalesById(id); }

    @PostMapping("/auth")
    public loginResponse authenticate(@RequestBody loginRequest loginRequest) {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword())
            );
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
            loginResponse failedResponse = new loginResponse();
            failedResponse.setMessage("Username and Password is not Exist!!");
            failedResponse.setStatus(403);
            failedResponse.setError("Invalid");
            return failedResponse;
        }

        return jwtUtil.generateToken(loginRequest.getUsername());

    }

}
