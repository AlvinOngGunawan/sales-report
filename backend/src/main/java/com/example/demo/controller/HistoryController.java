package com.example.demo.controller;

import com.example.demo.entity.History;
import com.example.demo.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class HistoryController {
    @Autowired
    private HistoryService service;

    @PostMapping("/addHistory")
    public History addHistory(@RequestBody History h){
        return service.addHisotry(h);
    }
}
